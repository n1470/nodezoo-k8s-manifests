# nodezoo-k8s-manifests

Manifests for different Nodezoo services.

## Getting started

For each new service to be monitored by Flux CD, do the following:

1. Create a new branch from `main`. The branch name should be the same name as the service name by convention, but can be anything that is unique among the services.
2. Create a new directory named `deploy`.
3. Add the k8s manifest file(s) for services and deployments to the `deploy` directory.
4. Create a `deploy/kustomization.yaml` with reference to the resources added in previous step.
